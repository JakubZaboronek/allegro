package com.jzaboron.allegro.services;

import com.jzaboron.allegro.model.Product;
import com.jzaboron.allegro.model.User;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    boolean registerProduct(Product productToRegister);

    Optional<Product> findByName(String name);


}
