package com.jzaboron.allegro.services;

import com.jzaboron.allegro.dao.TransactionDao;
import com.jzaboron.allegro.model.Transaction;
import com.jzaboron.allegro.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.logging.Logger;

@Service(value = "transactionService")
@Transactional
public class TransactionServiceImpl implements TransactionService {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(TransactionServiceImpl.class);
    @Autowired
    private TransactionDao transactionDao;

    public Transaction persistTransaction(User user, String toWallet, Double amount){
        return transactionDao.save(new Transaction(user, "wallet", toWallet, amount));
    }
}
