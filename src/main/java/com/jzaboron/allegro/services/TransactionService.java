package com.jzaboron.allegro.services;

import com.jzaboron.allegro.model.Transaction;
import com.jzaboron.allegro.model.User;

public interface TransactionService {
    Transaction persistTransaction(User userId, String toWallet, Double amount);
}
