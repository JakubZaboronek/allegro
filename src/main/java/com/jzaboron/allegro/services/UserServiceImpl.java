package com.jzaboron.allegro.services;

import com.jzaboron.allegro.dao.UserDao;
import com.jzaboron.allegro.model.User;
import com.jzaboron.allegro.model.UserData;
import com.sun.istack.internal.NotNull;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Override
    public boolean userExists(String login) {
        return userDao.userExists(login);
    }

    @Override
    public boolean registerUser(User userToRegister, UserData data) {
        userDao.registerUser(userToRegister, data);
        return true;
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public boolean userLogin(String login, String passwordHash) {
        // TODO: do zrobienia logowanie użytkowników
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<User> getUserWithId(Long id) {
        return userDao.findById(id);
    }

    @Override
    public void persistUser(User userTransfering) {
        userDao.save(userTransfering);
    }

    @Override
    public void postSomething(String post, User sender) {
    }

    @Override
    public Optional<User> findById(Long id) {
        return null;
    }
}
