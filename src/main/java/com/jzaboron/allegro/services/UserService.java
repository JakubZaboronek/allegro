package com.jzaboron.allegro.services;

import com.jzaboron.allegro.model.User;
import com.jzaboron.allegro.model.UserData;
import com.sun.istack.internal.NotNull;

import java.util.List;
import java.util.Optional;

public interface UserService {
    //user story chce sprawdzic czy dany uzytkownik o danym loginie juz istnieje ,wiec
    // ptrzebuje metody do sprawdzenia tego
    boolean userExists(String login);

    //user story-chce zarejestrowac uzytkownika
    boolean registerUser(User userToRegister, UserData userData);

    // user story chce wylistowac wszystkich uzytkownikow
    List<User> getAllUsers();

    //user story -logowanie uzytkownika
    boolean userLogin(String login, String passwordHash);

    void postSomething(String post, @NotNull User sender);

    Optional<User> findById(Long id);

    Optional<User> getUserWithId(Long id);

    void persistUser(User userTransfering);
}
