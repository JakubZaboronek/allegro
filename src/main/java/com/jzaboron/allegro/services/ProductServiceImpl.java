package com.jzaboron.allegro.services;

import com.jzaboron.allegro.dao.ProductDao;
import com.jzaboron.allegro.model.Product;
import com.jzaboron.allegro.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Service(value = "productService")
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public boolean registerProduct(Product productToRegister) {
        productDao.addProduct(productToRegister);
        return true;
    }

    @Override
    public Optional<Product> findByName(String name) {

        return productDao.findByName(name);
    }

}
