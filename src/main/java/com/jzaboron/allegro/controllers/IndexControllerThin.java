package com.jzaboron.allegro.controllers;

import com.jzaboron.allegro.model.User;
import com.jzaboron.allegro.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping(value = "/thinview/")
public class IndexControllerThin {

    @Autowired
    private UserService service;

    @RequestMapping(value = "/userProfile/{id}")
    public String getUserProfile(ModelMap model, @PathVariable Long id) {
        model.addAttribute("userid", id);
        Optional<User> user = service.getUserWithId(id);
        if (user.isPresent()) {
            User u = user.get();
            model.addAttribute("login", u.getLogin());
            model.addAttribute("passwordHash", u.getPasswordHash());
            model.addAttribute("firstName", u.getUserData().getFirstName());
            model.addAttribute("lastName", u.getUserData().getLastName());
            model.addAttribute("dateOfBirth", u.getUserData().getDateOfBirth());

        }
        return "UserProfileThin";
    }
}
