package com.jzaboron.allegro.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/thickview")
public class IndexControllerThick {

    @RequestMapping(value = "/getUserList")
    public String getUserList(){
        return "UserList";
    }

    @RequestMapping(value = "/userProfile/{id}")
    public String getUserProfile(ModelMap model, @PathVariable Long id){
        model.addAttribute("userid",id);
        return "UserProfileThick";
    }

}
