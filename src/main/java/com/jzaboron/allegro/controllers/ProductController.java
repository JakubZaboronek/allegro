package com.jzaboron.allegro.controllers;

import com.jzaboron.allegro.model.Product;
import com.jzaboron.allegro.model.responses.Response;
import com.jzaboron.allegro.model.responses.ResponseFactory;
import com.jzaboron.allegro.services.ProductService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/registerProduct", method = RequestMethod.GET)
    public ResponseEntity<Response> registerProduct(@RequestParam String name,
                                                    @RequestParam Double price) {
        Logger.getLogger(getClass()).debug("Requested registerProduct with params:" +
                " " + name + ":" + price);

        productService.registerProduct(new Product(name,price));
        return new ResponseEntity<Response>(
                ResponseFactory.success(),
                HttpStatus.OK
        );
    }

    @RequestMapping(value = "/product/{name}", method = RequestMethod.GET)
    public ResponseEntity<Response> getProduct(@PathVariable String name) {
        return new ResponseEntity<Response>(ResponseFactory.success(productService.findByName(name)), HttpStatus.OK);
    }
}
