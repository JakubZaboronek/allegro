package com.jzaboron.allegro.controllers;

import com.jzaboron.allegro.model.Transaction;
import com.jzaboron.allegro.model.User;
import com.jzaboron.allegro.model.UserData;
import com.jzaboron.allegro.model.responses.Response;
import com.jzaboron.allegro.model.responses.ResponseFactory;
import com.jzaboron.allegro.services.TransactionService;
import com.jzaboron.allegro.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value = "/registerUser", method = RequestMethod.GET)
    public ResponseEntity<Response> registerUser(@RequestParam String userName,
                                                 @RequestParam String password) {
        Logger.getLogger(getClass()).debug("Requested registerUser with params:" +
                " " + userName + ":" + password);

        // TODO: sprawdź czy użytkownik istnieje
        // TODO: jeśli nie istnieje użytkownik o podanym loginie, zarejestruj go
        if (userService.userExists(userName)) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User exists"),
                    HttpStatus.BAD_REQUEST);
        } else {
            userService.registerUser(new User(userName, password), new UserData("a", "b", "c", "d", LocalDate.now(), true));
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/listUsers", method = RequestMethod.GET)
    public ResponseEntity<List<User>> requestUserList() {
        return new ResponseEntity<List<User>>(userService.getAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> getUserWithId(@PathVariable Long id) {
        Optional<User> user = userService.getUserWithId(id);
        if (user.isPresent()) {
            return new ResponseEntity<Response>(ResponseFactory.success(user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/userExists/{login}", method = RequestMethod.GET)
    public ResponseEntity<Response> requestUserExists(@PathVariable String login) {
        return new ResponseEntity<Response>(ResponseFactory.success(userService.userExists(login)), HttpStatus.OK);
    }

    @RequestMapping(value = "/addTransaction", method = RequestMethod.GET)
    public ResponseEntity<Response> executeTransaction(@RequestParam Long fromUser,
                                                       @RequestParam String toWallet,
                                                       @RequestParam Double amount) {
        Optional<User> user = userService.getUserWithId(fromUser);
        if(user.isPresent()) {
            User userTransfering = user.get();
            Transaction transaction = transactionService.persistTransaction(userTransfering , toWallet, amount);
            userTransfering.getTransactionList().add(transaction);
            userService.persistUser(userTransfering);
            return new ResponseEntity<Response>(ResponseFactory.success(transaction), HttpStatus.OK);
        }else {
            return new ResponseEntity<Response>(ResponseFactory.failed("User with that id does not exist."), HttpStatus.BAD_REQUEST);
        }
    }
}
