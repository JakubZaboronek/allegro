package com.jzaboron.allegro.dao;

import com.jzaboron.allegro.model.Product;

import java.util.Optional;

public interface ProductDao {
    void addProduct(Product product);
    Optional<Product> findByName(String name);
}
