package com.jzaboron.allegro.dao;

import com.jzaboron.allegro.model.Transaction;
import org.springframework.stereotype.Repository;

@Repository(value = "transactionDao")
public class TransactionDaoImpl extends AbstractDao implements TransactionDao {
    @Override
    public Transaction save(Transaction transaction) {
        persist(transaction);
        return transaction;
    }
}
