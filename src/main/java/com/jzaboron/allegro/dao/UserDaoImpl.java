package com.jzaboron.allegro.dao;

import com.jzaboron.allegro.model.User;
import com.jzaboron.allegro.model.UserData;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository(value = "userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {
    @Override
    public void registerUser(User u, UserData data) {
        persist(data);
        u.setUserData(data);
        persist(u);
    }

    @Override
    public Optional<User> findById(Long id) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("id", id));

        return Optional.ofNullable((User) criteria.uniqueResult());
    }

    @Override
    public boolean userExists(String withLogin) {
        long resultCount = (long) getSession().createCriteria(User.class)
                .add(Restrictions.eq("login", withLogin).ignoreCase())
                .setProjection(Projections.rowCount()).uniqueResult();
        return resultCount > 0;
    }

    @Override
    public List<User> getAllUsers() {
        return getSession().createCriteria(User.class).list();
    }

    @Override
    public void save(User userTransfering) {
        super.save(userTransfering);
    }
}
