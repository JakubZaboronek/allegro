package com.jzaboron.allegro.dao;

import com.jzaboron.allegro.model.Transaction;

public interface TransactionDao {

    Transaction save(Transaction transaction);
}
