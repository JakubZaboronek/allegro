package com.jzaboron.allegro.dao;

import com.jzaboron.allegro.model.Transaction;
import com.jzaboron.allegro.model.User;
import com.jzaboron.allegro.model.UserData;
import com.sun.istack.internal.NotNull;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    void registerUser(User u, UserData data);

    Optional<User> findById(Long id);

    boolean userExists(String withLogin);

    List<User> getAllUsers();

    void save(User userTransfering);
}
