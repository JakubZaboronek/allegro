package com.jzaboron.allegro.dao;

import com.jzaboron.allegro.model.Product;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository(value = "productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {

    @Override
    public void addProduct(Product product) {
        getSession().persist(product);
    }

    @Override
    public Optional<Product> findByName(String name) {
        Criteria criteria = getSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("productName",name));

        return Optional.ofNullable((Product) criteria.uniqueResult());
    }
}
