package com.jzaboron.allegro.model;


import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "category")
    private String category;

    @Column(name = "price")
    private Double price;

    @Column(name = "state")
    private String state;


    public Product() {
        this.id = 0L;
    }

    public Product(String productName, String category, String state, Double price) {
        this.id = 0L;
        this.productName = productName;
        this.category = category;
        this.state = state;
        this.price = price;
    }
    public Product(String productName,Double price) {
        this.id = 0L;
        this.productName = productName;
        this.category = category;
        this.state = state;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", category='" + category + '\'' +
                ", state='" + state + '\'' +
                ", price=" + price +
                '}';
    }
}
