package com.jzaboron.allegro.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "user_transaction")
public class Transaction {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "app_user_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private User userFrom;

    @Column(nullable = false)
    private String walletAddressSender;

    @Column(nullable = false)
    private String walletAddressReceiver;

    @Column(nullable = false)
    private Double amount;

    public Transaction() {
        this.id = 0L;
    }

    public Transaction(User userIdFrom, String walletAddressSender, String walletAddressReceiver, Double amount) {
        this.id = 0L;
        this.userFrom = userIdFrom;
        this.amount = amount;
        this.walletAddressReceiver = walletAddressReceiver;
        this.walletAddressSender = walletAddressSender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(User userFrom) {
        this.userFrom = userFrom;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getWalletAddressSender() {
        return walletAddressSender;
    }

    public void setWalletAddressSender(String walletAddressSender) {
        this.walletAddressSender = walletAddressSender;
    }

    public String getWalletAddressReceiver() {
        return walletAddressReceiver;
    }

    public void setWalletAddressReceiver(String walletAddressReceiver) {
        this.walletAddressReceiver = walletAddressReceiver;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", userFrom=" + userFrom +
                ", amount=" + amount +
                '}';
    }
}
