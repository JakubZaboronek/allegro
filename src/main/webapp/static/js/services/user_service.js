'use strict';

angular.module('allegroapp').factory('UserService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/rest/';

        var factory = {
            fetchAllUsers: fetchAllUsers,
            fetchUserWithId: fetchUserWithId
        };
        console.log('Factory created.');
        return factory;

        function fetchAllUsers() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listUsers').then(
                function (data) {
                    // sukces
                    deferred.resolve(data);
                }, function (result) {
                    // nie udalo sie
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchUserWithId(id) {
            var deffered = $q.defer();
            $http.get(RESTAPIURL + 'user/' + id).then(
                function (data) {
                    //sukces
                    deffered.resolve(data);
                }, function (result) {
                    deffered.reject(result);
                }
            );
            return deffered.promise;
        }
    }]);