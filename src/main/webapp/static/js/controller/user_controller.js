'use strict';
angular.module('allegroapp').controller('UserController', ['$scope', 'UserService',
    function ($scope, UserService) {
        var self = this;
        self.users = [];
        self.user={
            "id": null,
            "login": "loading",
            "passwordHash": "loading...",
            "transactionList": [],
            "userData": {
                "id": null,
                "firstName": null,
                "lastName": null,
                "email": null,
                "address": null,
                "dateOfBirth": null,
                "female": false
            }
        };

        console.log('Controller created.');
        self.fetchAllUsersMethod = fetchAllUsers;
        self.fetchUserWithIdMethod = fetchUserWithId;

        fetchAllUsers();

        function fetchAllUsers() {
            UserService.fetchAllUsers().then(
                function (response) {
                    console.log(response);
                    self.users = response.data;
                }, // ^ resolve
                function (result) {
                    console.log(result);
                } // ^ reject
            );
        }

        function fetchUserWithId(userId) {
            console.log("Fetch user with id:" + userId);
            UserService.fetchUserWithId(userId).then(
                function (response) {
                    console.log(response);
                    self.user = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        $scope.onloadFunction = function (id) {
            console.log("fuwd" + id);
            fetchUserWithId(id);
        }
        function move(){
            var d=document.getElementById('moveable');
            d.style.position="absolute";
            d.style.left=d.style.left+10;
        }

    }]);